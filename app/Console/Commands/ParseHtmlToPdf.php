<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use LynX39\LaraPdfMerger\PDFManage;
use Yangqi\Htmldom\Htmldom;

class ParseHtmlToPdf extends Command
{

    public $adress_parse = 'https://voters.ng/';
    public $firstLevelStat = [];
    public $dir = '';
    public $state = '';
    public $localiti = '';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:htmlToPdf {state?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        if(filetype(public_path().'/savePdf/'.$this->argument('state')))


        if(!is_dir(public_path().'/savePdf/')){
            mkdir(public_path().'/savePdf/');
        }
        if(!is_dir(public_path().'/savePdf/'.$this->argument('state'))){
            mkdir(public_path().'/savePdf/'.$this->argument('state'));
        }

        $this->state = $this->argument('state');

        $this->parseState($this->argument('state'));



//        $html = new Htmldom($this->adress_parse);
//
//        foreach ($html->find('#menu808sjvvq p[align="left"] a') as $element){
//            $state = $this->parseDomainFromUrl($element->href);
//            echo 'State : ' . $state .'\\n';
//            mkdir(public_path().'/savePdf/'.$state);
//
//            $this->firstLevelStat[$state] = $this->parseState($state);
//            print_r($this->firstLevelStat);
//        }
//        print_r($this->firstLevelStat);
    }

    function parseDomainFromUrl($url){
        $parseUrl = parse_url($url);
        $domain = explode('.', $parseUrl['host']);
        return $domain[1];
    }

    function parseState($state){


        $list_localite = [];
//        $client = new \GuzzleHttp\Client();
//        echo $this->adress_parse.$state;
//        $client->setSslVerification(false);
//        $res = $client->get($this->adress_parse.$state);

        $url = $this->adress_parse.$state;
        $header = get_headers($url, 1);

        if($header[0] != 'HTTP/1.1 404 Not Found'){
            $html = new Htmldom($this->adress_parse.$state);
            foreach ($html->find('p[align="left"] a') as $element){
                $dirname = pathinfo($element->href);

                $parser_array = [];
                if(file_exists(public_path().'/savePdf/'.$this->state.'/parse.json')){
                    $parser_array = json_decode(file_get_contents(public_path().'/savePdf/'.$this->state.'/parse.json'));
                }
                if(!in_array($dirname['filename'], $parser_array)){
                    if($dirname['filename'] != 'fct'){
                        if(!is_dir(public_path().'/savePdf/'.$state.'/'.$dirname['filename'])){
                            mkdir(public_path().'/savePdf/'.$state.'/'.$dirname['filename']);
                        }

                        $this->localiti = $dirname['filename'];

                        $list_localite[] = [
                            'link_to_local' => $this->formatUrl($dirname['dirname']).rawurlencode($dirname['basename']),
                            'link_to_files' => $this->formatUrl($dirname['dirname'])
                        ];
                        echo 'Localite : ' . print_r($list_localite) .' \n';
                        $this->parseDomainFromState($this->adress_parse.$state.'/'.$this->formatUrl($dirname['dirname']), rawurlencode($dirname['basename']), $state, $dirname['filename']);
                    }
                }



            }
        }

        return $list_localite;

    }

    function formatUrl($url){
        $url_arr = explode('/', $url);
        $url_encode = '';

        foreach ($url_arr as $u){
            if($u != ''){
                $url_encode .= rawurlencode($u). '/';
            }
        }

        return $url_encode;
    }

    function parseDomainFromState($dir, $link, $state, $domain){

        $url = $dir.$link;
        $header = get_headers($url, 1);

        if($header[0] != 'HTTP/1.1 404 Not Found'){
            $html = new Htmldom($dir.$link);

            foreach ($html->find('select[class="frm"]') as $key => $element){

                if(!is_dir(public_path().'/savePdf/'.$state.'/'.$domain.'/select'.$key)){
                    mkdir(public_path().'/savePdf/'.$state.'/'.$domain.'/select'.$key);
                }

                $this->dir = public_path().'/savePdf/'.$state.'/'.$domain.'/select'.$key;
                foreach ($element->find('option') as $option){

                    $mystring = $option->value;
                    $findme   = 'Select';
                    $pos = strpos($mystring, $findme);

                    if ($pos === false) {
                        echo 'File : ' . $option->value .' \n';
                        $file_name = rawurlencode(strtolower(trim($option->value)));

                        if($file_name != ''){
                            $this->createPdfFromHtml($dir.$file_name.'.htm', strtolower(trim($option->value)), $dir);
                        }
                    }

                }
            }
        }

    }

    function createPdfFromHtml($link, $value, $dir){


        $url = $link;
        $header = get_headers($url, 1);

        if($header[0] != 'HTTP/1.1 404 Not Found'){

            $html_form_img = new Htmldom($url);
            $files_pdf = [];
            if(!is_dir($this->dir.'/'.$value)){
                mkdir($this->dir.'/'.$value);
            } else {
                $parser_array = [];
                if(file_exists(public_path().'/savePdf/'.$this->state.'/parse.json')){
                    $parser_array = json_decode(file_get_contents(public_path().'/savePdf/'.$this->state.'/parse.json'));
                }
                if(!in_array($this->localiti, $parser_array)){
                    $parser_array[] = $this->localiti;
                    file_put_contents(public_path().'/savePdf/'.$this->state.'/parse.json', json_encode($parser_array));
                }


                return ;
            }

            foreach ($html_form_img->find("body div[style^=position:absolute;left:50%;margin-left:-297px;]") as $key => $element){

                $pdf = App::make('dompdf.wrapper');
                $part_html = file_get_contents(public_path().'/parseHtml/header.html');
                $part_html .= '<div style="position:absolute;left:50%;margin-left:-297px;top: 0px;width: 595px;height:842px; border-style:outset; overflow:hidden;">';
                $part_html .= $element->innertext . '</div></body></html>';


                $str = 'qwertyuiopasdfghjkl';
                $shuffled = str_shuffle($str);

                $part_html = str_replace($value.'_files', $dir.$value.'_files', $part_html);


                file_put_contents($this->dir.'/'.$value.'/'.$shuffled.'.html', $part_html);

                echo 'Create pdf from link : ' . $link .' \n ';
    //            $pdf->loadHTML($part_html)
    //                ->setPaper('a4', 'portrait') // Portrait landscape
    //                ->setWarnings(false)
    ////                ->setOption('page-width', 200)
    ////                ->setOption('page-height', 200)
    //                ->save(public_path().'/parseHtml/'.$shuffled.'.pdf');
    //
    //            $files_pdf[] = public_path().'/parseHtml/'.$shuffled.'.pdf';
            }

            $parser_array = [];
            if(file_exists(public_path().'/savePdf/'.$this->state.'/parse.json')){
                $parser_array = json_decode(file_get_contents(public_path().'/savePdf/'.$this->state.'/parse.json'));
            }

            if(!in_array($this->localiti, $parser_array)){
                $parser_array[] = $this->localiti;
                file_put_contents(public_path().'/savePdf/'.$this->state.'/parse.json', json_encode($parser_array));
            }


        }


//        $pdf = new PdfManage;
//
//        foreach($files_pdf as $file){
//            $pdf->addPDF($file, 'all');
//
//        }
//
//        $pdf->merge('file', $this->dir.'/'.$value.'.pdf', 'P');
//
//        foreach($files_pdf as $file){
//            unlink($file);
//        }

    }

}
